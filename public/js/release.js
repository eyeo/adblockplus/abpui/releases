"use strict";

const announcementLabels = {
  "internal": "Internal release announcement",
  "notes-draft": "Internal release notes draft",
  "notes": "Release notes",
  "public": "Release announcement",
  "updates": "Updates page"
};

const gitlabProjects = {
  abp: {
    label: "Adblock Plus",
    path: "adblockinc/ext/adblockplus/adblockplus"
  },
  core: {
    label: "Core",
    path: "eyeo/adblockplus/abc/adblockpluscore"
  },
  ewe: {
    label: "EWE",
    path: "eyeo/adblockplus/abc/webext-ad-filtering-solution"
  },
  snippets: {
    label: "Snippets",
    path: "eyeo/anti-cv/snippets"
  },
  ui: {
    label: "UI",
    path: "adblockinc/ext/adblockplus/adblockplusui"
  },
  webext: {
    label: "WebExt",
    path: "eyeo/adblockplus/adblockpluschrome"
  }
};

async function getValidVersions()
{
  const resp = await fetch("./data/index.json");
  const {releases} = await resp.json();
  const versions = releases.map((release) => release.version);
  return new Set(versions);
}

async function getVersion()
{
  const match = /.*\bversion=(next|\d+(?:\.\d+){1,2})\b/.exec(location.search);
  if (!match)
    throw new Error("No valid version specified");

  const validVersions = await getValidVersions();
  const [, version] = match;
  if (!validVersions.has(version))
    throw new Error("Unknown version number");

  return version;
}

function getReference(id)
{
  let label;
  let url;

  if (id.includes("#"))
  {
    const [projectId, issueId] = id.split("#", 2);
    const project = gitlabProjects[projectId];

    label = `${project.label} #${issueId}`;
    url = `https://gitlab.com/${project.path}/-/issues/${issueId}`
  }
  else if (id.includes("%"))
  {
    const [projectId, milestoneId] = id.split("%", 2);
    const project = gitlabProjects[projectId];

    label = `${project.label} milestone "${milestoneId}"`;
    url = `https://gitlab.com/${project.path}/-/milestones?utf8=%E2%9C%93&search_title=${encodeURIComponent(milestoneId)}&state=all`;
  }

  return {label, url};
}

function setApproved(id, isApproved)
{
  $(`#approvals .approval-${id}`).classList.toggle("approved", isApproved);
}

function renderNotesText(text, links)
{
  const fragment = document.createDocumentFragment();

  const linkMatch = /^(.*?)\[([^\]]+)\](?:\[([^\]]+)\])?(.*)$/.exec(text);
  if (linkMatch)
  {
    const [, textBefore, linkText, linkRef, textAfter] = linkMatch;

    let url = null;
    if (linkRef)
    {
      url = links[linkRef];
    }
    else
    {
      const [, projectId, issueId] = /^(.*)#(\d+)$/.exec(linkText);
      switch (projectId)
      {
        case "abp":
          url = `https://gitlab.com/adblockinc/ext/adblockplus/adblockplus/-/issues/${issueId}`;
          break;
        case "core":
          url = `https://gitlab.com/eyeo/adblockplus/abc/adblockpluscore/-/issues/${issueId}`;
          break;
        case "ewe":
          url = `https://gitlab.com/eyeo/adblockplus/abc/webext-ad-filtering-solution/-/issues/${issueId}`;
          break;
        case "snippets":
          url = `https://gitlab.com/eyeo/anti-cv/snippets/-/issues/${issueId}`;
          break;
        case "ui":
          url = `https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/issues/${issueId}`;
          break;
        case "webext":
          url = `https://gitlab.com/eyeo/adblockplus/adblockpluschrome/-/issues/${issueId}`;
          break;
      }
    }

    fragment.appendChild(renderNotesText(textBefore, links));

    const link = create(fragment, "a");
    link.href = url;
    link.appendChild(renderNotesText(linkText, links));

    fragment.appendChild(renderNotesText(textAfter, links));
  }
  else
  {
    const codeMatch = /^(.*?)`([^`]+)`(.*)$/.exec(text);
    if (codeMatch)
    {
      const [, textBefore, textCode, textAfter] = codeMatch;

      fragment.appendChild(renderNotesText(textBefore, links));

      const code = create(fragment, "code");
      code.textContent = textCode;

      fragment.appendChild(renderNotesText(textAfter, links));
    }
    else
    {
      fragment.textContent = text;
    }
  }

  return fragment;
}

function renderNotesMultilineItem(item, links)
{
  const [itemTitle, ...itemNotes] = item;
  const fragment = document.createDocumentFragment();

  const title = create(fragment, "div");
  title.appendChild(renderNotesText(itemTitle, links));

  for (const itemNote of itemNotes)
  {
    const note = create(fragment, "div");
    note.classList.add("note");
    note.appendChild(renderNotesText(itemNote, links));
  }

  return fragment;
}

function renderNotesGroup(group, links, level = 0)
{
  const fragment = document.createDocumentFragment();

  if (group.title)
  {
    const groupTitle = create(fragment, (level === 0) ? "h4" : "span");
    groupTitle.appendChild(renderNotesText(group.title, links));

    if (group.subtitle && level === 0)
    {
      const groupSubtitle = create(fragment, "p");
      groupSubtitle.appendChild(renderNotesText(group.subtitle, links));
    }
  }

  const changes = create(fragment, "ul");
  for (const item of group.items || [])
  {
    const change = create(changes, "li");

    let content = null;
    if (item instanceof Array)
    {
      content = renderNotesMultilineItem(item, links);
    }
    else if (item instanceof Object)
    {
      content = renderNotesGroup(item, links, level + 1);
    }
    else
    {
      content = renderNotesText(item, links);
    }
    change.appendChild(content);
  }

  return fragment;
}

function renderNotes(notes)
{
  const fragment = document.createDocumentFragment();

  if (notes.summary)
  {
    const summary = [notes.summary].flat();
    for (const summaryLine of summary)
    {
      const line = create(fragment, "p");
      line.appendChild(renderNotesText(summaryLine, notes.links));
    }
  }

  if (notes.changes)
  {
    for (const group of notes.changes)
    {
      fragment.appendChild(renderNotesGroup(group, notes.links));
    }
  }

  return fragment;
}

async function run()
{
  const version = await getVersion();
  const resp = await fetch(`./data/releases/${version}.json`, {cache: "no-store"});
  const releaseInfo = await resp.json();

  const title = `Adblock Plus ${releaseInfo.version}`;
  $("title").textContent += ` | ${title}`;
  $("h1").textContent = title;

  for (const platform of releaseInfo.platforms)
  {
    const image = create($("#platforms"), "img");
    image.src = `./images/platforms/${platform}.svg`;
  }

  const phase = getCurrentPhase(releaseInfo.milestones);
  const phaseLabel = createPhaseLabel(phase);
  $("#current").appendChild(phaseLabel);
  document.body.dataset.phase = phase;

  for (const approvalId in releaseInfo.approvals)
  {
    if (approvalId === "stores")
    {
      for (const storeId in releaseInfo.approvals[approvalId])
      {
        setApproved(storeId, !!releaseInfo.approvals[approvalId][storeId]);
      }
      continue;
    }

    setApproved(approvalId, !!releaseInfo.approvals[approvalId]);
  }

  $("#progress .phase-development .date").textContent = releaseInfo.milestones.created;
  $("#progress .phase-featurefreeze .date").textContent = releaseInfo.milestones.featurefreeze;
  $("#progress .phase-codefreeze .date").textContent = releaseInfo.milestones.codefreeze;
  $("#progress .phase-storereview .date").textContent = releaseInfo.milestones.release;
  $("#progress .phase-closed .date").textContent = releaseInfo.milestones.closed;

  for (const refId of releaseInfo.scope)
  {
    const reference = getReference(refId);

    const element = create($("#scope"), "li");
    const link = create(element, "a");
    link.href = reference.url;
    link.textContent = reference.label;
  }

  for (const announcementId in releaseInfo.announcements)
  {
    if (!releaseInfo.announcements[announcementId])
      continue;

    const element = create($("#announcements"), "li");
    const link = create(element, "a");
    link.href = releaseInfo.announcements[announcementId];
    link.textContent = announcementLabels[announcementId];
  }

  $("#log").textContent = releaseInfo.logText;
  $(".notes").appendChild(renderNotes(releaseInfo.notes));

  if (releaseInfo.contents)
  {
    $("#contents").href = releaseInfo.contents;
    $("#contents").hidden = false;
  }
}

run().catch((err) =>
{
  alert(err.message);
  console.error(err);
});
