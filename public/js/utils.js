"use strict";

const $ = (selector, parent = document) => parent.querySelector(selector);

function create(parent, tagName)
{
  const element = document.createElement(tagName);
  parent.appendChild(element);
  return element;
}

function getDate(str)
{
  if (!str || !/^\d{4}-\d{2}-\d{2}$/.test(str))
    return null;

  return new Date(str);
}

function getCurrentPhase(milestones)
{
  const now = new Date();
  const created = getDate(milestones.created);
  const featurefreeze = getDate(milestones.featurefreeze);
  const codefreeze = getDate(milestones.codefreeze);
  const release = getDate(milestones.release);
  const closed = getDate(milestones.closed);

  let phase;
  if (closed && now > closed)
  {
    phase = "closed";
  }
  else if (release && now > release)
  {
    phase = "storereview";
  }
  else if (codefreeze && now > codefreeze)
  {
    phase = "codefreeze";
  }
  else if (featurefreeze && now > featurefreeze)
  {
    phase = "featurefreeze";
  }
  else if (created && now > created)
  {
    phase = "development";
  }
  else
  {
    phase = "planned";
  }

  return phase;
}

function createPhaseLabel(phase)
{
  const element = $("#phase-label").content.cloneNode(true);
  $(".phase-label", element).dataset.phase = phase;
  return element;
}
