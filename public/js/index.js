"use strict";

const MS_IN_DAY = 24 * 60 * 60 * 1000;

function renderLegend()
{
  const legend = $("#legend");
  legend.appendChild(createPhaseLabel("planned"));
  legend.appendChild(createPhaseLabel("development"));
  legend.appendChild(createPhaseLabel("featurefreeze"));
  legend.appendChild(createPhaseLabel("codefreeze"));
  legend.appendChild(createPhaseLabel("storereview"));
  legend.appendChild(createPhaseLabel("closed"));

  const blockerLabel = create(legend, "span");
  blockerLabel.classList.add("blocker-label");
  blockerLabel.textContent = "Release blocker"
  legend.appendChild(blockerLabel);
}

function getUTC(date, isEndOfDay = false)
{
  if (!date)
    return null;

  if (typeof date === "string" && !/^\d{4}-\d{2}-\d{2}$/.test(date))
    return null;

  const moment = vis.moment(date);
  if (isEndOfDay)
    return moment.endOf("day").toDate();

  return moment.toDate();
}

function renderChart(releases, blockers)
{
  const now = Date.now();
  const groups = new vis.DataSet();
  const items = new vis.DataSet();

  for (const release of releases)
  {
    const {milestones} = release;

    let groupLabel = document.createElement("div");
    groupLabel.innerHTML = `
      <span class="phase-dot phase-${getCurrentPhase(milestones)}"></span>
      <a href="release.html?version=${release.version}">
        ${release.version}
      </a>
    `;
    groups.add({
      id: release.version,
      content: groupLabel
    });

    if (!getUTC(milestones.created))
      continue;

    if (milestones.created !== milestones.featurefreeze)
    {
      items.add({
        id: `${release.version}#development`,
        group: release.version,
        start: getUTC(milestones.created),
        end: getUTC(milestones.featurefreeze) || getUTC(now),
        className: "release phase-development"
      });
    }

    if (!getUTC(milestones.featurefreeze))
      continue;

    if (milestones.featurefreeze !== milestones.codefreeze)
    {
      items.add({
        id: `${release.version}#featurefreeze`,
        group: release.version,
        start: getUTC(milestones.featurefreeze),
        end: getUTC(milestones.codefreeze) || getUTC(now),
        className: "release phase-featurefreeze"
      });
    }

    if (!getUTC(milestones.codefreeze))
      continue;

    if (milestones.codefreeze !== milestones.release)
    {
      items.add({
        id: `${release.version}#codefreeze`,
        group: release.version,
        start: getUTC(milestones.codefreeze),
        end: getUTC(milestones.release) || getUTC(now),
        className: "release phase-codefreeze"
      });
    }

    if (!getUTC(milestones.release))
      continue;

    if (milestones.release !== milestones.closed)
    {
      items.add({
        id: `${release.version}#storereview`,
        group: release.version,
        start: getUTC(milestones.release),
        end: getUTC(milestones.closed, true) || getUTC(now),
        className: "release phase-storereview"
      });
    }
  }

  const options = {
    start: new Date(now - 6 * 30 * MS_IN_DAY),
    end: new Date(now + 30 * MS_IN_DAY),
    margin: {axis: 0, item: 0},
    orientation: "top",
    stack: false,
    horizontalScroll: true,
    zoomKey: "ctrlKey",
    zoomMin: 14 * MS_IN_DAY,
    zoomMax: 10 * 365 * MS_IN_DAY,
    loadingScreenTemplate: () => "Loading releases…"
  };

  for (const blocker of blockers)
  {
    let [start, end, name] = blocker;
    items.add({
      id: `blocker-${start}`,
      content: name || "",
      start: getUTC(start),
      end: getUTC(end, true),
      type: "background",
      className: "blocker"
    });
  }

  new vis.Timeline($("#timeline"), items, groups, options);
}

async function run()
{
  const resp = await fetch("data/index.json", {cache: "no-store"});
  const {blockers, releases} = await resp.json();
  releases.reverse();

  renderLegend();
  renderChart(releases, blockers);
}

run().catch((err) =>
{
  alert(err.message);
  console.error(err);
});
