"use strict";

const phases = [
  "storereview",
  "codefreeze",
  "featurefreeze",
  "development"
];

const stores = [
  "google",
  "microsoft",
  "mozilla",
  "opera"
];

function renderLegend()
{
  const legend = $("#legend");
  legend.appendChild(createPhaseLabel("development"));
  legend.appendChild(createPhaseLabel("featurefreeze"));
  legend.appendChild(createPhaseLabel("codefreeze"));
  legend.appendChild(createPhaseLabel("storereview"));
}

function renderDurations(releases, onlyPhase)
{
  const groups = new vis.DataSet();

  for (let i = 0; i < phases.length; i++)
  {
    groups.add({
      id: `phase-${i}`,
      className: `release phase-${phases[i]}`,
      visible: !onlyPhase || onlyPhase === phases[i]
    });
  }

  for (let i = 0; i < stores.length; i++)
  {
    groups.add({
      id: `store-${i}`,
      className: `release store-${stores[i]}`,
      visible: onlyPhase === "storereview" || onlyPhase === stores[i],
      options: {excludeFromStacking: true}
    });
  }

  const items = [];
  let x = 0;
  for (const release of releases)
  {
    const label = {
      content: release.version,
      yOffset: -5
    };

    for (let i = 0; i < phases.length; i++)
    {
      const phase = phases[i];
      if (!(phase in release.durations))
        continue;

      items.push({
        x,
        y: release.durations[phase],
        group: `phase-${i}`,
        end: x + 1 + stores.length - 1,
        label: (onlyPhase || phase === "development") ? label: null
      });
    }

    for (let i = 0; i < stores.length; i++)
    {
      items.push({
        x,
        y: release.durations[stores[i]],
        group: `store-${i}`,
        end: x + 1,
        label: (onlyPhase === stores[i]) ? label : null
      });

      x++;
    }

    x++;
  }

  const options = {
    style: "bar",
    stack: true,
    barChart: {align: "center", sideBySide: true},
    dataAxis: {
      left: {
        range: {min: 0},
        title: {text: "Days"}
      }
    },
    drawPoints: {size: 0},
    orientation: "bottom",
    start: -1,
    end: x - 1,
    min: -1,
    max: x - 1,
    moveable: false,
    zoomable: false,
    timeAxis: {scale: "year"}
  };
  new vis.Graph2d($("#durations"), items, groups, options);
}

async function run()
{
  const resp = await fetch("data/performance.json", {cache: "no-store"});
  const {releases} = await resp.json();

  renderLegend();

  const params = new URLSearchParams(location.search);
  renderDurations(releases, params.get("filter"));
}

run().catch((err) =>
{
  alert(err.message);
  console.error(err);
});
