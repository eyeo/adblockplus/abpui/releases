Release specification
=====================

- [Manifest](#manifest)
- [Announcements](#announcements)
- [Log](#log)
- [Identifiers](#identifiers)

## Manifest

- Version number (e.g. 1.2.3)
- Platforms  
  List of platforms that the release is available on. One or more of:
  - `chrome`
  - `edge`
  - `firefox`
  - `opera`
- Scope  
  List of [issues and project milestones](#identifiers) that are blocking
  the release
- Milestones
  - Creation date  
    _entering development phase_
  - Feature freeze date  
    _entering testing phase_
  - Code freeze date  
    _entering release phase_
  - Release date  
    _entering store review phase_
  - Closing date
- Required approvals (optionally with date)
  - Scope
  - Notes
  - QA
  - Stores
    - AMO
    - CWS
    - Microsoft store
    - Opera store
- [Announcements](#announcement)
  - internal
  - notes-draft
  - notes
  - public
  - updates
- Contents

## Announcements

Describes changes in release. Primary changes are described in summary and
other changes are listed in further sections. Contains links to
[issues](#identifiers) related to each change.

- Summary
- Changes
  - Title
  - Subtitle
  - Items
- Links

| Name | Location | Contents |
|-|-|-|
| Internal announcement | Intranet | All information relevant to team |
| Notes | [adblockplus.org/releases][abp-releases] | All information relevant to any user |
| Updates page | Included in extension | Some information relevant to all users |
| Public announcement | [adblockplus.org/blog][abp-blog] | Some information relevant to any user |

## Log

Records steps that were taken as part of the release process, as well as deviations and encountered problems.

## Identifiers

### Internal projects

- Issue syntax: `project "#" issue-id` (e.g. [ui#123][ui123])
- Milestone syntax: `project "%" project-id`

Projects:
- [abp][project-abp]
- [core][project-core]
- [snippets][project-snippets]
- [ui][project-ui]
- [webext][project-webext]

### External projects

- Issue syntax: `project " bug " issue-id` (e.g. [Firefox bug 1234567][firefox-1234567])

Projects:
- [chromium][project-chromium]
- [firefox][project-firefox]



[abp-blog]: https://adblockplus.org/blog/
[abp-releases]: https://adblockplus.org/releases/
[project-abp]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplus/-/issues/
[project-chromium]: https://bugs.chromium.org/p/chromium/issues/list
[project-core]: https://gitlab.com/eyeo/adblockplus/abc/adblockpluscore/-/issues/
[project-firefox]: https://bugzilla.mozilla.org/
[project-snippets]: https://gitlab.com/eyeo/adblockplus/abp-snippets/-/issues/
[project-ui]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/issues/
[project-webext]: https://gitlab.com/eyeo/adblockplus/adblockpluschrome/-/issues/
[firefox-1234567]: https://bugzilla.mozilla.org/show_bug.cgi?id=1234567
[ui123]: https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/issues/123
