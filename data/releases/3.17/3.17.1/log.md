- 2023-06-12
  - Defined scope.
  - Created release notes draft.
  - Published internal release announcement.
- 2023-06-16
  - Received notification that testing has finished.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    On Debian or Ubuntu: Run the following commands using Node 16.10.0 and npm 8:
      npm install --fetch-retries=0
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Published to Microsoft store.
  - Updated extension source code URL in Opera store.
  - Published to Opera store.
  - Noticed that release is live on Microsoft store.
- 2023-06-19
  - Noticed that release is live on CWS.
  - Noticed that release is live on Opera store.
- 2023-06-20
  - Noticed that release is live on AMO.
