- 2023-04-14
  - Defined scope.
  - Published internal release announcement.
  - Created release notes draft.
- 2023-04-25
  - Received notification that testing has finished.
  - CI pipeline failed due to failing `apt-get update` command.
    https://gitlab.com/adblockinc/ext/adblockplus/adblockplusui/-/jobs/4174756345
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Agreed with QA to ignore failing CI job and instead rely on other safeguards in release workflow.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    On Debian or Ubuntu: Run the following commands using Node 16.10.0 and npm 8:
      npm install --fetch-retries=0
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Published to Microsoft store.
  - Updated extension source code URL in Opera store.
  - Published to Opera store.
- 2023-04-26
  - Noticed that release is live on CWS.
  - Noticed that release is live on Microsoft store.
- 2023-04-27
  - Noticed that release is live on Opera store.
- 2023-05-05
  - Noticed that release is live on AMO.
- 2023-06-16
  - Published to downloads repo.
