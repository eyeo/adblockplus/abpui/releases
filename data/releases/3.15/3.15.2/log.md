- 2022-12-01
  - Defined scope.
  - Published internal release announcement.
  - Created release notes draft.
- 2022-12-02
  - Received notification that testing has finished.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
- 2022-12-05
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    Run the following commands using Node 16.10.0:
      npm install --legacy-peer-deps
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Noticed that release is live on AMO.
  - Published to Microsoft store.
  - Updated extension source code URL and build instructions in Opera store.
  - Published to Opera store.
  - Published to downloads repo.
- 2022-12-06
  - Noticed that release is live on CWS.
  - Noticed that release is live on Opera store.
  - Received notification that Microsoft store review failed:
    1 &nbsp failure(s)
    The certification for your extension has failed. See review report for details.
  - Published again to Microsoft store with note for reviewers:
    In response to the reported Critical Validation failure, I'm resubmitting the same version again.
    
    The reason for this action being that neither the notification email nor the report itself provided any information on what the problem was with the extension version. Furthermore, there are indications that the report was made in error, because the failure was described as "1 &nbsp failure(s)" and the link to the report led to the download of an HTML file without any content and with broken references to other files.
- 2022-12-14
  - Noticed that release is live on Microsoft store.
