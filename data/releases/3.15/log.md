- 2022-09-21
  - Defined scope.
  - Published internal release announcement.
  - Created release notes draft.
- 2022-11-23
  - Received notification that testing has finished.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    Run the following commands using Node 16.10.0:
      npm install --legacy-peer-deps
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Noticed that release is live on AMO.
  - Published to Microsoft store.
  - Updated extension source code URL in Opera store.
  - Published to Opera store with new permission "Access to one or more hosts (not all)".
- 2022-11-24
  - Noticed that release is live on CWS.
  - Noticed that release is live on Microsoft store.
  - Noticed that release is live on Opera store.
- 2022-12-05
  - Published to downloads repo.
