- 2022-06-29
  - Defined scope.
  - Published internal release announcement.
  - Created release notes draft.
- 2022-06-30
  - Received notification that testing has finished.
- 2022-07-04
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
- 2022-07-05
  - Created MR for downloads repo.
  - Failed to publish to CWS. Requires declaring whether publisher account is considered a trader or non-trader.
  - Published to AMO with note for reviewers:
    Run the following commands using Node 16.10.0:
      npm install
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Published to Microsoft store.
  - Updated extension source code URL in Opera store.
  - Published to Opera store.
  - Received approval for declaring CWS publisher account as trader.
  - Published to CWS.
  - Noticed that release is live on AMO.
  - Noticed that release is live on Opera store.
  - Noticed that release is live on Microsoft store.
  - Noticed that release is live on CWS.
- 2022-07-26
  - Published to downloads repo.
