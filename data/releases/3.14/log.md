- 2021-06-16
  - Defined scope.
- 2022-01-11
  - Redefined scope.
- 2022-05-03
  - Published internal release announcement.
  - Created release notes draft.
- 2022-05-27
  - Received notification that testing has finished.
- 2022-05-30
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
- 2022-05-31
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    Run the following commands using Node 16.10.0:
      npm install
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Noticed that release is live on AMO.
  - Published to Microsoft store.
  - Updated extension source code URL in Opera store.
  - Published to Opera store.
  - Noticed that release is live on CWS.
  - Noticed that release is live on Microsoft store.
- 2022-06-02
  - Noticed that release is live on Opera store.
- 2022-07-26
  - Published to downloads repo.
