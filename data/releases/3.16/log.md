- 2023-01-09
  - Defined scope.
  - Published internal release announcement.
  - Created release notes draft.
- 2023-01-25
  - Received notification that testing has finished.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    Run the following commands using Node 16.10.0:
      npm install
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Published to Microsoft store.
  - Updated extension source code URL in Opera store.
  - Published to Opera store.
- 2023-01-26
  - Noticed that release is live on Opera store.
  - Received notification that Microsoft store review failed:
    The extension description does not provide information about the type of in-product purchases offered.
  - Received notification that AMO review failed due to problems when building the extension.
  - Responded to AMO reviewer with instructions.
- 2023-01-27
  - Published again to Microsoft store after updating description on store page.
  - Noticed that release is live on CWS.
  - Updated release notes.
- 2023-01-30
  - Noticed that release is live on Microsoft store.
- 2023-02-10
  - Noticed that release is live on AMO.
