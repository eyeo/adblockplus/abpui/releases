- 2023-02-21
  - Defined scope.
- 2023-03-03
  - Published internal release announcement.
  - Created release notes draft.
- 2023-03-08
  - Received notification that testing has finished.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    On Debian or Ubuntu: Run the following commands using Node 16.10.0 and npm 8:
      npm install --fetch-retries=0
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Published to Microsoft store.
  - Updated extension source code URL and build instructions in Opera store.
  - Published to Opera store.
  - Noticed that release is live on Microsoft store.
- 2023-03-09
  - Noticed that release is live on CWS.
  - Noticed that release is live on Opera store.
- 2023-03-14
  - Received notification that AMO review failed due to problems when building the extension.
  - Responded to AMO reviewer with instructions.
- 2023-03-23
  - Noticed that release is live on AMO.
