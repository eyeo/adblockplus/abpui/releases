- 2023-11-06
  - Defined scope.
  - Published internal release announcement.
  - Created release notes draft.
- 2023-11-09
  - Received notification that testing has finished.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to Microsoft store.
  - Updated extension source code URL in Opera store.
  - Published to Opera store.
- 20223-11-10
  - Noticed that release is live on CWS.
  - Noticed that release is live on Microsoft store.
  - Noticed that release is live on Opera store.
