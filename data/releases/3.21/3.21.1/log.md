- 2023-11-14
  - Set preliminary feature freeze date to 2023-11-22.
  - Set preliminary release date to 2023-11-28.
- 2023-11-22
  - Defined scope.
  - Published internal release announcement.
  - Create release notes draft.
- 2023-11-27
  - Received notification that testing has finished.
  - Published to downloads repo.
  - Published to CWS.
  - Published to Microsoft store.
  - Published to Opera store.
- 2023-11-29
  - Noticed that release is live on CWS.
  - Noticed that release is live on Microsoft store.
  - Noticed that relesae is live on Opera store.
