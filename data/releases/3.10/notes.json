{
  "summary": "This release includes various improvements to the \"Block element\" feature, adds ad blocking support for more languages and [restricts the ability to subscribe to filter lists to some trusted websites][blog-restrictions]. It also drops support for some old browser versions, namely Chromium 59 and Firefox 51.",
  "changes": [
    {
      "title": "User interface changes",
      "items": [
        "Redesigned \"Block element\" dialog and added ability to preview blocked elements ([ui#638], [ui#761]).",
        "Open a web-based version of the first-run page upon installation ([ui#833]).",
        "Updated design of buttons in icon popup ([ui#472]).",
        "Updated design of rating button in settings page ([ui#706]).",
        "Added donation button to settings page and first-run page ([ui#769], [ui#818]).",
        "Removed Weibo link from settings page ([ui#670]).",
        "Allow Chinese users to share number of blocked items on Weibo ([ui#642]).",
        "Fixed: Cannot add new filter list by pressing Enter key ([ui#548]).",
        "Fixed: Hide counters in icon popup when for non-web pages ([ui#810]).",
        "Fixed: Hide \"Block element\" button in icon popup for web pages in allowlist ([ui#806]).",
        "Fixed: Settings page layout broke when displaying lengthy language names ([ui#793])."
      ]
    },
    {
      "title": "Filter changes",
      "items": [
        {
          "title": "Snippet changes",
          "items": [
            "Removed `readd` snippet ([core#179]).",
            "Protected `freeze-element` snippet from outside interference ([core#260])."
          ]
        },
        "Restricted subscribe links to trusted websites ([webext#87], [webext#263]).",
        "Recommend Dandelion Sprout's Nordic Filters filter list for Nordic languages ([core#233], [ui#841]).",
        "Recommend IndianList filter list for some additional languages ([core#245], [ui#841]).",
        "Fixed: Preinstalled incorrect language filter list if no suitable one was found ([webext#271])."
      ]
    },
    {
      "title": "Other changes",
      "items": [
        "Check for data corruption upon initialization ([ui#830], [webext#201]).",
        "Fixed: Don't show notifications to user in case of data corruption ([ui#829], [core#247], [webext#244]).",
        "Fixed: Avoid interfering with web browsing when opening a new tab ([ui#831], [webext#260])."
      ]
    },
    {
      "title": "Chromium-specific changes",
      "items": [
        [
          "Dropped support for Chromium 59 and below ([webext#266]).",
          "This includes Chrome 59 and Opera 46."
        ],
        "Fixed: Tabs opened by user were treated as popups ([webext#272])."
      ]
    },
    {
      "title": "Firefox-specific changes",
      "items": [
        "Dropped support for Firefox 51 ([webext#143]).",
        "Fixed: Scrolling through custom filters was slow ([ui#487]).",
        "Fixed: Firefox freezes when using element hiding debug mode ([core#220], [webext#209]).",
        "Fixed: Frames are not being collapsed ([webext#90])."
      ]
    },
    {
      "title": "Opera-specific changes",
      "items": [
        "Fixed: Cannot close \"Block element\" dialog ([ui#791])."
      ]
    }
  ],
  "links": {
    "blog-restrictions": "https://adblockplus.org/blog/restricting-the-use-of-subscribe-links-to-prevent-circumvention"
  }
}
