- 2022-03-25
  - Published internal release announcement.
  - Created release notes draft.
- 2022-04-25
  - Received notification that testing has finished.
- 2022-05-02
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Failed to verify that sitekey filters work.
- 2022-05-03
  - Received approval to release as-is on May 12.
- 2022-05-12
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    Run the following commands using Node 16.10.0:
      npm install
      cd adblockpluschrome
      npx gulp build -t firefox -c release
  - Noticed that release is live on AMO.
  - Published to Microsoft store.
  - Published to Opera store with note for reviewers:
    Run the following commands using Node 16.10.0:
      npm install
      cd adblockpluschrome
      npx gulp build -t chrome -c release
- 2022-05-13
  - Noticed that release is live on CWS.
  - Noticed that release is live on Opera store.
  - Published to downloads repo.
- 2022-05-20
  - Noticed that release is live on Microsoft store.
