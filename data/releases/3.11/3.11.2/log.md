- 2021-07-30
  - Received notification of upcoming snippet changes.
- 2021-08-17
  - Received notification that testing has started.
  - Published internal release announcement.
  - Created release notes draft.
- 2021-08-20
  - Received notification that testing has finished.
- 2021-08-31
  - Fast-forwarded master onto dep-snippets-2021-2 branch.
  - Received confirmation that testing has finished.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
- 2021-09-01
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    In preparation for Manifest v3, we've been making some changes to our internal dependencies. So for the time being, you need to build the extension from within the "adblockpluschrome" subdirectory:
      cd adblockpluschrome
      npm install
      npx gulp build -t firefox -c release
  - Published to Microsoft store
    - Error after uploading file: Something went wrong. Please try again.
  - Published to Opera store.
  - Published release notes.
  - Noticed that release is live on AMO.
  - Noticed that release is live on CWS.
- 2021-09-03
  - Noticed that release is live on Microsoft store.
- 2021-09-07
  - Noticed that release is stuck in review on Opera store.
- 2021-09-08
  - Published to downloads repo.
  - Replied to Opera store reviewers with a link to the source code archive.
- 2021-09-30
  - Noticed that release is live on Opera store.
