{
  "summary": [
    "This release adds an easy way to block cookie warnings and push notifications. For advanced users, it also introduces the ability to block requests based on their headers as well as an option for debugging element hiding filters.",
    "Note that the extension is no longer sending GET requests to check for updates for disabled filter lists. Instead, it is sending [HEAD requests][mdn-http-head] to save bandwidth while they're not in use."
  ],
  "changes": [
    {
      "title": "User interface changes",
      "items": [
        "Added new filter list recommendations for blocking cookie warnings and push notifications ([ui#701], [core#248]).",
        "Added checkbox for enabling debug element hiding filters mode ([ui#608]).",
        "Refreshed Adblock Plus logo color ([ui#819]).",
        "Ignore private tabs when counting blocked requests ([ui#851], [ui#899]).",
        "Hide issue reporter and share buttons for private tabs ([ui#870], [ui#886])."
      ]
    },
    {
      "title": "Filter changes",
      "subtitle": "Upgraded adblockpluscore to 0.3.0 ([release notes][core-0.3.0]), which includes the following changes:",
      "items": [
        {
          "title": "Snippet changes",
          "items": [
            "Removed `uabinject-defuser` snippet ([core#287])."
          ]
        },
        "Added support for [`$header` filter option][help-header] ([core#77], [ui#898], [webext#194]).",
        "Update checks for disabled filter lists are now being made using HEAD requests and occur more frequently ([core#290])."
      ]
    },
    {
      "title": "Chromium-specific changes",
      "items": [
        "Fixed: Unable to print on some pages ([webext#287], [Chromium bug 1138634][chromium-1138634])."
      ]
    },
    {
      "title": "Firefox-specific changes",
      "items": [
        "Improved compatibility with Firefox Android 79+ ([ui#868])."
      ]
    }
  ],
  "links": {
    "chromium-1138634": "https://bugs.chromium.org/p/chromium/issues/detail?id=1138634",
    "core-0.3.0": "https://gitlab.com/eyeo/adblockplus/abc/adblockpluscore/-/releases/0.3.0",
    "help-header": "https://help.eyeo.com/en/adblockplus/how-to-write-filters?766f1691#header-filter",
    "mdn-http-head": "https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD"
  }
}
