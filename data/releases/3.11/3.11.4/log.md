- 2021-11-16
  - Received notification of abp-snippets 0.3.2 being ready for integration.
- 2021-11-18
  - Published internal release announcement.
  - Created release notes draft.
  - Received notification that testing has finished.
- 2021-11-23
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    In preparation for Manifest v3, we've been making some changes to our internal dependencies. So for the time being, you need to build the extension from within the "adblockpluschrome" subdirectory:
      cd adblockpluschrome
      npm install
      npx gulp build -t firefox -c release
  - Published to Microsoft store.
  - Published to Opera store.
  - Published release notes.
  - Noticed that release is live on AMO.
  - Published to downloads repo.
- 2021-11-24
  - Noticed that release is live on CWS.
  - Noticed that release is live on Microsoft store.
- 2021-12-02
  - Noticed that release is live on Opera store.
