- 2021-02-12
  - Received notification of upcoming snippet changes.
- 2021-05-26
  - Decided on only introducing the new, private snippets dependency as part of the release, following it up with an actual snippets release.
- 2021-07-20
  - Received notification that testing has started.
  - Published internal release announcement.
  - Created release notes draft.
- 2021-07-22
  - Received notification that testing has finished.
- 2021-07-26
  - Fast-forwarded master onto dep-snippets-2021-1 branch.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
- 2021-07-27
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers:
    In preparation for Manifest v3, we've been making some changes to our internal dependencies. So for the time being, you need to build the extension from within the "adblockpluschrome" subdirectory:
      cd adblockpluschrome
      npm install
      npx gulp build -t firefox -c release

    This update introduces a minified snippets.min.js file, whose source code is included in the source archive.
  - Noticed that release is live on AMO.
  - Published to Microsoft store
    - Error after uploading file: Something went wrong. Please try again.
    - Note for reviewers:
      In preparation for Manifest v3, we've been making some changes to our internal dependencies. So for the time being, you need to build the extension from within the "adblockpluschrome" subdirectory:
        cd adblockpluschrome
        npm install
        npx gulp build -t firefox -c release
  - Published to Opera store.
  - Published release notes.
  - Noticed that release is live on CWS.
  - Published to downloads repo.
  - Noticed that release is live on Opera store.
- 2021-08-03
  - Noticed that release is live on Microsoft store.
