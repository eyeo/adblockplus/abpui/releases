- 2020-09-24
  - Asked for non-binding plans for changes to include in release.
- 2021-02-13
  - Received notification about upcoming adblockpluscore 0.3.0 release on 2021-02-22.
- 2021-02-26
  - Published internal release announcement.
- 2021-03-05
  - Created release notes draft.
- 2021-04-08
  - Set release date based on donation campaign schedule.
- 2021-04-19
  - Moved release date to May 19 following the extension of the ongoing donation campaign.
- 2021-04-20
  - Received notification that testing has finished.
- 2021-05-18
  - Triggered "webext" CI child pipeline. Platform tests all failed with "Testpages > Filters > WebSocket Exception > Error: Screenshots don't match".
  - Investigated cause and found that error also occurs when rerunning successful past pipeline and that running test succeeds when done manually. Got confirmation from QA that failure is safe to ignore and that it might be caused by quota limits with echo server that's used for tests.
- 2021-05-19
  - Fast-forwarded master onto release-2020-1 branch.
  - Created build files.
  - Verified that building Firefox extension from source archive yields same XPI file.
  - Created MR for downloads repo.
  - Published to CWS.
  - Published to AMO with note for reviewers: In preparation for Manifest v3, we've been making some changes to our internal dependencies. So for the time being, you need to build the extension from within the "adblockpluschrome" subdirectory, which used to be the root directory in previous versions.
  - Noticed that release is live on AMO.
  - Published to Microsoft store despite error after uploading file: Something went wrong. Please try again.
  - Published to Opera store. Encountered error on first upload attempt: Upload error has occured: 500 Internal Server Error
  - Noticed that release is live on CWS.
  - Noticed that release is live on Opera store.
  - Published release notes.
- 2021-05-25
  - Published to downloads repo.
- 2021-05-26
  - Noticed that release is live on Microsoft store.
