"use strict";

const fs = require("fs");
const path = require("path");
const saxon = require("saxon-js");
const {promisify} = require("util");

const fsReadFile = promisify(fs.readFile);
const fsWriteFile = promisify(fs.writeFile);

const {crawl} = require("./crawler");

const dirpathInput = "./data/releases/";
const dirpathOutput = "./dist/announcements/";

const [, , inputVersion, format, visibility] = process.argv;

if (!inputVersion || !/^\d+(?:\.\d+)+$/.test(inputVersion))
{
  console.error(`Invalid version specified: ${inputVersion}`);
  process.exit(1);
}

async function output(content)
{
  const filepathOutput = path.join(
    dirpathOutput,
    `${inputVersion}-${visibility}.${format}`
  );
  await fsWriteFile(filepathOutput, content, "utf8");
  console.log(`Successfully written file: ${filepathOutput}`);
}

async function run()
{
  const releaseInfos = await crawl([], "3", dirpathInput);
  const [release] = releaseInfos
    .filter((releaseInfo) => releaseInfo.version === inputVersion);

  // Collect data
  const json = {
    visibility,
    release
  };
  if (format === "json")
    return output(JSON.stringify(json, null, 2));

  // Convert to XML
  const json2xml = await fsReadFile("./tmp/json2xml.sef.json", "utf8");
  const {principalResult: xml} = await saxon.transform({
    stylesheetText: json2xml,
    // Ampersands in JSON are interpreted as character references, causing
    // conversion to fail, so we need to manually convert them first
    sourceText: `<data>${JSON.stringify(json).replace(/&/g, "&amp;")}</data>`,
    destination: "serialized"
  });
  if (format === "xml")
    return output(xml);

  // Generate page structure
  const xml2xhtml = await fsReadFile("./tmp/xml2xhtml.sef.json", "utf8");
  const {principalResult: xhtml} = await saxon.transform({
    stylesheetText: xml2xhtml,
    sourceText: xml,
    destination: "serialized"
  });
  if (format === "xhtml")
    return output(xhtml);

  // Generate code
  if (format !== "md" && format !== "txp")
    return;

  const xhtml2code = await fsReadFile(
    `./tmp/xhtml2${format}.sef.json`,
    "utf8"
  );
  const {principalResult: code} = await saxon.transform({
    stylesheetText: xhtml2code,
    sourceText: xhtml,
    destination: "serialized"
  });
  return output(code);
}
run().catch(console.error);
