<?xml version="1.0"?>
<!-- Based on https://gist.github.com/gabetax/1702774 -->
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:functx="http://www.functx.com"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="xs" version="3.0">
  <xsl:output method="text" indent="no" omit-xml-declaration="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:function name="functx:indent" as="xs:string">
    <xsl:param name="count" as="xs:integer"/>
    <xsl:sequence select="string-join((for $i in 1 to $count return '  '), '')"/>
  </xsl:function>

  <xsl:template match="head/title"></xsl:template>

  <xsl:template match="h1">
    <xsl:text># </xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="h4">
    <xsl:text>#### </xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>&#xa;&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="em">
    <xsl:text>_</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>_</xsl:text>
  </xsl:template>

  <xsl:template match="strong">
    <xsl:text>**</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>**</xsl:text>
  </xsl:template>

  <xsl:template match="li/div">
    <xsl:apply-templates select="*|text()"/>
    <xsl:if test="position() < last()">
      <xsl:text>  &#xa;</xsl:text>
      <xsl:value-of select="functx:indent(count(ancestor::li))"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="div">
    <xsl:apply-templates select="*|text()"/>
    <xsl:text>&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="p">
    <xsl:if test="not(preceding-sibling::p)">
      <xsl:text>&#xa;</xsl:text>
    </xsl:if>
    <xsl:apply-templates select="*|text()"/>
    <xsl:text>&#xa;&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="body/ul">
    <xsl:apply-templates select="* except text()"/>
    <xsl:text>&#xa;&#xa;</xsl:text>
  </xsl:template>

  <xsl:template match="ul">
    <xsl:apply-templates select="* except text()"/>
  </xsl:template>

  <xsl:template match="li">
    <xsl:if test="position() > 1 or count(ancestor::li) > 0">
      <xsl:text>&#xa;</xsl:text>
    </xsl:if>
    <xsl:value-of select="functx:indent(count(ancestor::li))"/>
    <xsl:text>- </xsl:text>
    <xsl:apply-templates select="node()|text()"/>
  </xsl:template>

  <xsl:template match="a">
    <xsl:text>[</xsl:text>
    <xsl:apply-templates select="node()|text()"/>
    <xsl:text>](</xsl:text>
    <xsl:value-of select="@href"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="code">
    <xsl:text>`</xsl:text>
    <xsl:apply-templates select="node()|text()"/>
    <xsl:text>`</xsl:text>
  </xsl:template>
</xsl:stylesheet>
